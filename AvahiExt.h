//
//  AvahiExt.h
//  AvahiExt
//
//  Created by Dan Kalinin on 11/25/20.
//

#import <AvahiExt/AvhMain.h>
#import <AvahiExt/AvhInit.h>

FOUNDATION_EXPORT double AvahiExtVersionNumber;
FOUNDATION_EXPORT const unsigned char AvahiExtVersionString[];
